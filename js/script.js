const ServicesLi = document.querySelectorAll(`.Services_items`);
const img_text = document.querySelectorAll(`.img_text`);
ServicesLi.forEach((elem) => {
    elem.addEventListener(`click`, (event) => {
        ServicesLi.forEach((element) => element.classList.remove(`active_services`))
        event.target.classList.add(`active_services`);
        img_text.forEach((elem) => elem.style.display = ` none`)
        const data = event.target.dataset.serv;
        document.getElementById(data).style.display = ` flex`;
    })
});

const btnFoto = document.querySelector(`.button_foto`);
const foto = document.querySelectorAll(`.foto`);
btnFoto.addEventListener(`click`, (event) => {
    const loader = document.createElement(`DIV`)
    loader.classList.add(`anim`)
    btnFoto.after(loader)
    btnFoto.remove()
    setTimeout(() => {
        foto.forEach((elem) => {
            loader.remove()
            elem.style.position = `static`;
            elem.style.zIndex = `auto`;
            elem.style.opacity = `1`;
            btnFoto.remove()
        })
    }, 3000)

});

const amazing_items = document.querySelectorAll(`.Amazing_items`);
document.querySelector(`.Amazing_items`).style.border = ` 1px solid #18CFAB `;
amazing_items.forEach((elem) => {
    elem.addEventListener(`click`, (event) => {
        const dataT = event.target.dataset.t;
        amazing_items.forEach((elem) => elem.style.border = `none`)
        event.target.style.border = ` 1px solid #18CFAB `;
        foto.forEach((elem) => {
            elem.classList.remove(`f_none`);
            if (!elem.classList.contains(dataT) && dataT !== `All`) {
                elem.classList.add(`f_none`);
            }
        })
    })
});

const clicerLeft = document.querySelector(`.clicer_left`);
const clicerRight = document.querySelector(`.clicer_right`);
const aboutItems = document.querySelectorAll(`.about_theHam_items img`);
const aboutContant = document.querySelectorAll(`.about_theHam_contant`);
const aboutFirst = document.querySelector(`.about_theHam_first`);
let indexNum = 0;
let size = 490;



clicerRight.addEventListener(`click`, (event) => {
    if (indexNum >= aboutItems.length -1) {
        indexNum = -1;
    }
    indexNum++
    aboutItems.forEach((elem, index) => {
        elem.classList.remove(`up`)
        if (indexNum == index) {
            elem.classList.add(`up`)
            document.querySelectorAll(`.about_theHam_contant`).forEach((elem)=>{
                elem.style.transform = ` translateY( ${ -size * indexNum}px ) `
            })
        }
  
    })
})
clicerLeft.addEventListener(`click`, (event) => {
    indexNum--
    aboutItems.forEach((elem, index) => {
        elem.classList.remove(`up`)
        if (indexNum < 0) {
            indexNum = aboutItems.length - 1;
        }

        if (indexNum == index) {
            console.log(1111);
            console.log(elem);
            elem.classList.add(`up`)
            document.querySelectorAll(`.about_theHam_contant`).forEach((elem)=>{
                elem.style.transform = ` translateY( ${ -size * indexNum}px ) `
            })
        }
     
    })
})
